# Curso Descomplicando DevOps

## Introdução

Auxiliar o pessoal que já trabalhou como dev e está em migração de carreira para DevOps, entre outros.
Falar de conceitos, infraestrutura como código, infraestrutura imutável, entender como essas ferramentas se aplicam na prática.

Mostrar como funcioma na prática, vamos lidar com uma série de ferramentas, geralmente as mais utilizadas, vamos falar sobre docker, kubernestes, terraform, prometheus, helm, ansible, packer, entre outros.

Vamos ver várias ferramentas trabalhando juntas pra entregar o básico, uma esteira pipeline, do início ao fim.

## Material de apoio

Materiais de apoio

- [Docker-101](https://caiodelgado.dev/docker-101/)
- [05 - Docker compose](https://github.com/caiodelgadonew/docker/blob/main/manuscript/05-docker-compose.md)
- [Docker para desenvolvedores](https://github.com/gomex/docker-para-desenvolvedores)
- [Devops challenge](https://github.com/juancbdm/devops_challenge)
- [Smaller docker images](https://learnk8s.io/blog/smaller-docker-images)
- [Guia foca](https://guiafoca.org/)
- [Docker build](https://docs.docker.com/engine/reference/builder)
- [Scratch](https://hub.docker.com/_/scratch)
- [Trivy](https://github.com/aquasecurity/trivy)
- [Docker wordpress](https://docs.docker.com/samples/wordpress/)
- [Dockerizing with docker compose](https://stavshamir.github.io/python/dockerizing-a-flask-mysql-app-with-docker-compose/)
- [Django](https://docs.docker.com/samples/django/)
- [NodeJs](https://github.com/docker/labs/tree/master/developer-tools/nodejs/porting/)
- [NodeJS Docker Webapp](https://nodejs.org/en/docs/guides/nodejs-docker-webapp/)
- [Builder](https://docs.docker.com/engine/reference/builder/#add)
- [Docker compose](https://docs.docker.com/compose/)
- [Docke compose file v3](https://docs.docker.com/compose/compose-file/compose-file-v3/)

## Comandos: Docker

```sh
user@linux:~$ docker ps
user@linux:~$ docker images
user@linux:~$ docker system prune
user@linux:~$ docker build -t docker-linuxtips .
user@linux:~$ docker rmi --force $(docker images -aq)
user@linux:~$ docker images -aq
user@linux:~$ docker run --help
user@linux:~$ docker run -p 8080:8080 --name docker-node-4 -d docker-linuxtips
user@linux:~$ docker volume
user@linux:~$ docker network
```

O exercício foi realizado baseado na documentação oficial [Doc NodeJS](https://nodejs.org/en/docs/guides/nodejs-docker-webapp/)

O código do projeto pode ser baixado de: [Projeto](https://github.com/camilla-m/docker-node-linuxtips)

## Dockerizando o primeiro app
